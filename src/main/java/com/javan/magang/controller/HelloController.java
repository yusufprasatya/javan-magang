package com.javan.magang.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HelloController {

    @GetMapping({"/hello", "/"})
    public ModelAndView hello() {
        ModelAndView mav = new ModelAndView("hello");
        return mav;
    }

}
