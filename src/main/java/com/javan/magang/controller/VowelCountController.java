package com.javan.magang.controller;

import com.javan.magang.model.Operation;
import com.javan.magang.model.Sentence;
import com.javan.magang.service.VowelCountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class VowelCountController {
    Sentence sentence = new Sentence();

    @Autowired
    private VowelCountService vowelCountService;

    @RequestMapping("/vowel-count")
    public String getVowelCountPage(Model model){
        model.addAttribute("sentence", sentence);
        return "vowels";
    }

    @RequestMapping(value="/vowel-count", params="cek", method = RequestMethod.POST)
    public String getOddEven(@ModelAttribute("sentence")  Sentence sentence, Model model ){
        model.addAttribute("results", vowelCountService.checkVowel(sentence));
        return "vowels";
    }

}
