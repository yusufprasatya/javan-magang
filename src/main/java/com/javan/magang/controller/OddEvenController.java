package com.javan.magang.controller;

import com.javan.magang.model.Operation;
import com.javan.magang.service.OddEvenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class OddEvenController {
    Operation operationModel = new Operation();

    @Autowired
    private OddEvenService oddEvenService;

    @RequestMapping("/oddeven")
    public String getCalculatorPage(Model model){
        model.addAttribute("operation", operationModel);
        return "oddeven";
    }

    @RequestMapping(value="/oddeven", params="cek", method = RequestMethod.POST)
    public String getOddEven(@ModelAttribute("operation")  Operation operationModel, Model model ){
        model.addAttribute("results", oddEvenService.checkNumber(operationModel));
        return "oddeven";
    }

}
