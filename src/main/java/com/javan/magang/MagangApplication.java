package com.javan.magang;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MagangApplication {

	public static void main(String[] args) {
		SpringApplication.run(MagangApplication.class, args);
	}

}
