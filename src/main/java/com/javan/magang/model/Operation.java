package com.javan.magang.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Operation {
    private String operation;
    private int a;
    private int b;

    public Operation(String operation) {
        this.operation = operation;
    }
}