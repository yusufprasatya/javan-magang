package com.javan.magang.service.impl;

import com.javan.magang.model.Operation;
import com.javan.magang.service.OddEvenService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class OddEvenServiceImpl implements OddEvenService {

    @Override
    public List<String> checkNumber(Operation model) {
        int firstNumber = model.getA();
        int lastNumber = model.getB();
        List<String> oddEvens = new ArrayList<>();


        for (int i = firstNumber; i <= lastNumber ; i++) {
            if (i % 2 == 0){
                System.out.println("Angka "+i +" adalah genap");
                oddEvens.add("Angka "+i +" adalah genap");
            }else {
                System.out.println("Angka "+i +" adalah ganjil");
                oddEvens.add("Angka "+i +" adalah ganjil");
            }
        }
        return oddEvens;
    }
}
