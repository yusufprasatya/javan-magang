package com.javan.magang.service.impl;

import com.javan.magang.model.Operation;
import com.javan.magang.service.CalculatorService;
import org.springframework.stereotype.Service;

@Service
public class CalculatorServiceImpl implements CalculatorService {
    public int add(Operation model){
        return model.getA() + model.getB();
    }

    public int subtract(Operation model){
        return model.getA() - model.getB();
    }

    public int multiply(Operation model){
        return model.getA() * model.getB();
    }

    public double divide(Operation model){
        if(model.getA() == 0) return 0;
        if(model.getB() == 0) return 0;
        return (double) model.getA() / model.getB();
    }

    public Operation clearSimple(Operation model){
        model.setA(0);
        model.setB(0);
        return model;
    }

}