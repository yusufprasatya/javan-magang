package com.javan.magang.service.impl;

import com.javan.magang.model.Sentence;
import com.javan.magang.service.VowelCountService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class VowelCountServiceImpl implements VowelCountService {

    @Override
    public String checkVowel(Sentence sentence) {
        String str = sentence.getText().toLowerCase();
        Character[] vowels = {'a','i','u','e','o'};
        List<Character> vowelList = new ArrayList<>();

        for (int i=0; i<str.length(); i++) {
            for (Character ch: vowels) {
                if (str.charAt(i) == ch){
                    if (!vowelList.contains(ch)) {
                        vowelList.add(str.charAt(i));
                    }
                }
            }
        }
       return sentence.getText() +" jumlah huruf vokalnya " +vowelList.size() +
                " yaitu " + vowelList.toString();
    }
}
