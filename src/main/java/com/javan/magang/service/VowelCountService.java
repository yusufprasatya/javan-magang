package com.javan.magang.service;

import com.javan.magang.model.Sentence;

public interface VowelCountService {
    public String checkVowel(Sentence text);
}
