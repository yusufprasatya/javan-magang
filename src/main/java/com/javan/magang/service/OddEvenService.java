package com.javan.magang.service;

import com.javan.magang.model.Operation;

import java.util.List;

public interface OddEvenService {
    public List<String> checkNumber(Operation model);
}
