package com.javan.magang.service;

import com.javan.magang.model.Operation;

public interface CalculatorService {
    public int add(Operation model);
    public int subtract(Operation model);
    public int multiply(Operation model);
    public double divide(Operation model);
    public Operation clearSimple(Operation model);
}
